package com.concretesolutions.javapop.expressotests;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;
import android.view.View;

import com.concretesolutions.javapop.R;
import com.concretesolutions.javapop.expressotests.mocks.Mocks;
import com.concretesolutions.javapop.providers.GithubAPI;
import com.concretesolutions.javapop.providers.GithubAPIFactory;
import com.concretesolutions.javapop.ui.activities.RepositoriesActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.vidageek.mirror.config.Item;
import net.vidageek.mirror.dsl.Mirror;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


/**
 * Created by LeandroCesar on 26/03/2017.
 */

@RunWith(AndroidJUnit4.class)
public class RepositoriesActivityTest  {

    private MockWebServer server;

    @Rule
    public ActivityTestRule<RepositoriesActivity> mActivityRule = new ActivityTestRule<>(RepositoriesActivity.class, true, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        GithubAPIFactory.BASE_URL = server.url("/").toString();
    }

    @Test
    public void testLoadList() throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(Mocks.SUCCESS_REPOSITORY));

        Intent intent = new Intent();
        mActivityRule.launchActivity(intent);

        onView(withId(R.id.repository_recycler_view)).check(matches(isDisplayed()));
    }


    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }
}