package com.concretesolutions.javapop;

import com.concretesolutions.javapop.models.PullRequest;
import com.concretesolutions.javapop.utils.PullRequestsUtils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by LeandroCesar on 26/03/2017.
 */

public class PullRequestUtilsTest {

    @Test
    public void count_pull_request_by_state() throws Exception {
        List<PullRequest> pullRequestList = new ArrayList<>();

        PullRequest pullRequest = new PullRequest();
        pullRequest.setId(1l);
        pullRequest.setState("closed");
        pullRequestList.add(pullRequest);

        pullRequest = new PullRequest();
        pullRequest.setId(2l);
        pullRequest.setState("open");
        pullRequestList.add(pullRequest);

        pullRequest = new PullRequest();
        pullRequest.setId(3l);
        pullRequest.setState("closed");
        pullRequestList.add(pullRequest);

        Map countPullRequest = PullRequestsUtils.getCountByState(pullRequestList);

        assertEquals(2, countPullRequest.get("closed"));
        assertEquals(1, countPullRequest.get("open"));
        assertEquals(null, countPullRequest.get("close"));
    }

}
