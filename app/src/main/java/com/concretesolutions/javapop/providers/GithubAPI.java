package com.concretesolutions.javapop.providers;

import com.concretesolutions.javapop.models.PullRequest;
import com.concretesolutions.javapop.models.Repository;
import com.concretesolutions.javapop.models.User;
import com.concretesolutions.javapop.utils.ListWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by LeandroCesar on 25/03/2017.
 */

public interface GithubAPI {

    @GET("/search/repositories?q=language:Java&sort=stars")
    Call<ListWrapper<Repository>> getRepositories(@Query("page") Integer page);

    @GET("/users/{login}")
    Call<User> getUser(@Path("login") String username);

    @GET("/repos/{user}/{repository}/pulls")
    Call<PullRequest[]> getPullRequests(@Path("user") String username, @Path("repository") String repository);
}
