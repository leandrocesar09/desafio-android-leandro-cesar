package com.concretesolutions.javapop.providers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by LeandroCesar on 25/03/2017.
 */

public class GithubAPIFactory {
    public static String BASE_URL = "https://api.github.com";

    public static GithubAPI getApi() {
        Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();

        Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

        return retrofit.create(GithubAPI.class);

    }


}
