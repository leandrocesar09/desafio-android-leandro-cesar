package com.concretesolutions.javapop.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by LeandroCesar on 25/03/2017.
 */

public class Repository implements Serializable {

    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    private String description;

    @SerializedName("owner")
    private User owner;

    @SerializedName("forks_count")
    private Long forksCount;

    @SerializedName("stargazers_count")
    private Long starsCount;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Long getForksCount() {
        return forksCount;
    }

    public void setForksCount(Long forksCount) {
        this.forksCount = forksCount;
    }

    public Long getStarsCount() {
        return starsCount;
    }

    public void setStarsCount(Long starsCount) {
        this.starsCount = starsCount;
    }

    @Override
    public String toString() {
        return "Repository{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", forksCount=" + forksCount +
                ", starsCount=" + starsCount +
                '}';
    }
}
