package com.concretesolutions.javapop.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.concretesolutions.javapop.R;
import com.concretesolutions.javapop.models.PullRequest;
import com.concretesolutions.javapop.models.Repository;
import com.concretesolutions.javapop.ui.activities.PullRequestsActivity;
import com.concretesolutions.javapop.utils.CircleTransform;
import com.concretesolutions.javapop.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by LeandroCesar on 25/03/2017.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestViewHolder>{

    private Context context;
    private List<PullRequest> pullRequests;

    public PullRequestAdapter(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }


    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pull_request, parent, false);
        return new PullRequestAdapter.PullRequestViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, int position) {
        PullRequest pullRequest = pullRequests.get(position);
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        holder.txtTitlePullRequest.setText(pullRequest.getTitle());
        holder.txtBodyPullRequest.setText(pullRequest.getBody());
        holder.txtUsernamePullRequest.setText(pullRequest.getUser().getUsername());
        holder.txtNamePullRequest.setText(pullRequest.getUser().getName());
        holder.txtDatePullRequest.setText(formato.format(pullRequest.getDate()));
        Picasso.with(context).load(pullRequest.getUser().getAvatarUrl()).transform(new CircleTransform()).into(holder.imgUserPullRequest);

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Uri uri = Uri.parse(pullRequests.get(position).getUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount(){
        return pullRequests.size();
    }



    public static class PullRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        ItemClickListener clickListener;

        @BindView(R.id.txtTitlePullRequest)
        TextView txtTitlePullRequest;

        @BindView(R.id.txtBodyPullRequest)
        TextView txtBodyPullRequest;

        @BindView(R.id.imgUserPullRequest)
        ImageView imgUserPullRequest;

        @BindView(R.id.txtUsernamePullRequest)
        TextView txtUsernamePullRequest;

        @BindView(R.id.txtNamePullRequest)
        TextView txtNamePullRequest;

        @BindView(R.id.txtDatePullRequest)
        TextView txtDatePullRequest;

        public PullRequestViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }

}
