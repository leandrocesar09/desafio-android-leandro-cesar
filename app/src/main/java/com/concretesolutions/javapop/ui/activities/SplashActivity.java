package com.concretesolutions.javapop.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.concretesolutions.javapop.utils.NetworkUtils;

/**
 * Created by LeandroCesar on 26/03/2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (NetworkUtils.isConnectingToInternet(getApplicationContext())){
            Intent intent = new Intent(this, RepositoriesActivity.class);
            startActivity(intent);
            finish();
        }else{
            Toast toast = Toast.makeText(getBaseContext(), "Cheque sua conexão e tente novamente.", Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
