package com.concretesolutions.javapop.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.concretesolutions.javapop.R;
import com.concretesolutions.javapop.models.Repository;
import com.concretesolutions.javapop.ui.activities.PullRequestsActivity;
import com.concretesolutions.javapop.utils.CircleTransform;
import com.concretesolutions.javapop.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by LeandroCesar on 25/03/2017.
 */

public class RepositoryAdapter extends RecyclerView.Adapter {


    private Context context;
    private List<Repository> repositories;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public RepositoryAdapter(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder mViewHolder;
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository, parent, false);
            mViewHolder = new RepositoryViewHolder(view);
        }else{
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progressbar, parent, false);
            mViewHolder = new ProgressBarViewHolder(view);
        }
        return mViewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RepositoryViewHolder){
            Repository repository = repositories.get(position);
            ((RepositoryViewHolder)holder).txtUsernameOwnerRepository.setText(repository.getOwner().getUsername());
            ((RepositoryViewHolder)holder).txtNameRepository.setText(repository.getName());
            ((RepositoryViewHolder)holder).txtDescriptionRepository.setText(repository.getDescription());
            ((RepositoryViewHolder)holder).txtNameOwnerRepository.setText(repository.getOwner().getName());
            ((RepositoryViewHolder)holder).txtForksCountRepository.setText(String.valueOf(repository.getForksCount()));
            ((RepositoryViewHolder)holder).txtStarsCountRepository.setText(String.valueOf(repository.getStarsCount()));

            Picasso.with(context).load(repository.getOwner().getAvatarUrl()).transform(new CircleTransform()).into(((RepositoryViewHolder)holder).imgOwnerRepository);

            ((RepositoryViewHolder)holder).setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    Intent intent = new Intent(context, PullRequestsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("repository", repositories.get(position));
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });

        }else{
            ((ProgressBarViewHolder)holder).scrollProgressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount(){
        return repositories.size() + 1;
    }


    @Override
    public int getItemViewType(int position) {
        return (position >= repositories.size()) ? VIEW_PROG : VIEW_ITEM;
    }

    public static class RepositoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        ItemClickListener clickListener;

        @BindView(R.id.txtUsernameOwnerRepository)
        TextView txtUsernameOwnerRepository;

        @BindView(R.id.txtNameRepository)
        TextView txtNameRepository;

        @BindView(R.id.imgOwnerRepository)
        ImageView imgOwnerRepository;

        @BindView(R.id.txtDescriptionRepository)
        TextView txtDescriptionRepository;

        @BindView(R.id.txtNameOwnerRepository)
        TextView txtNameOwnerRepository;

        @BindView(R.id.txtForksCountRepository)
        TextView txtForksCountRepository;

        @BindView(R.id.txtStarsCountRepository)
        TextView txtStarsCountRepository;

        public RepositoryViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }

    }

    public static class ProgressBarViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.scroll_progressbar)
        ProgressBar scrollProgressBar;

        public ProgressBarViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }



    }

}
