package com.concretesolutions.javapop.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.concretesolutions.javapop.R;
import com.concretesolutions.javapop.models.Repository;
import com.concretesolutions.javapop.providers.GithubAPI;
import com.concretesolutions.javapop.providers.GithubAPIFactory;
import com.concretesolutions.javapop.ui.adapters.RepositoryAdapter;
import com.concretesolutions.javapop.utils.EndlessRecyclerViewScrollListener;
import com.concretesolutions.javapop.utils.ListWrapper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepositoriesActivity extends AppCompatActivity {

    private RepositoryAdapter mRepositoryAdapter;
    private GithubAPI mAPI;
    private List<Repository> repositoryList = new ArrayList<>();

    @BindView(R.id.repository_recycler_view)
    RecyclerView mRepositoryRecyler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);

        ButterKnife.bind(this);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRepositoryRecyler.setLayoutManager(mLayoutManager);
        mRepositoryRecyler.setHasFixedSize(true);

        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mAPI.getRepositories(page+1).enqueue(repositoriesCallback);
            }
        };
        mRepositoryRecyler.addOnScrollListener(scrollListener);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRepositoryRecyler.getContext(),
                mLayoutManager.getOrientation());
        mRepositoryRecyler.addItemDecoration(dividerItemDecoration);

        mAPI = GithubAPIFactory.getApi();
        mAPI.getRepositories(1).enqueue(repositoriesCallback);

    }

    Callback<ListWrapper<Repository>> repositoriesCallback = new Callback<ListWrapper<Repository>>() {
        @Override
        public void onResponse(Call<ListWrapper<Repository>> call, Response<ListWrapper<Repository>> response) {
            if (response.isSuccessful()) {
                ListWrapper<Repository> repositories = response.body();
                repositoryList.addAll(repositories.getItems());
                if (mRepositoryAdapter == null){
                    mRepositoryAdapter = new RepositoryAdapter(repositoryList);
                    mRepositoryRecyler.setAdapter(mRepositoryAdapter);
                    mRepositoryRecyler.setVisibility(View.VISIBLE);
                }else{
                    mRepositoryAdapter.setRepositories(repositoryList);
                    mRepositoryAdapter.notifyDataSetChanged();
                }
            } else {
                Log.d("RepositoriesCallback", "Code: " + response.code() + " Message: " + response.message());
            }
        }

        @Override
        public void onFailure(Call<ListWrapper<Repository>> call, Throwable t) {
            t.printStackTrace();
        }
    };



}
