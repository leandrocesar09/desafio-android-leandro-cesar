package com.concretesolutions.javapop.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.concretesolutions.javapop.R;
import com.concretesolutions.javapop.models.PullRequest;
import com.concretesolutions.javapop.models.Repository;
import com.concretesolutions.javapop.providers.GithubAPI;
import com.concretesolutions.javapop.providers.GithubAPIFactory;
import com.concretesolutions.javapop.ui.adapters.PullRequestAdapter;
import com.concretesolutions.javapop.utils.PullRequestsUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequestsActivity extends AppCompatActivity {

    private PullRequestAdapter mPullRequestAdapter;
    private GithubAPI mAPI;

    @BindView(R.id.pull_request_recycler_view)
    RecyclerView mPullRequestRecyler;

    @BindView(R.id.txtOpeneds)
    TextView txtOpeneds;

    @BindView(R.id.txtCloseds)
    TextView txtCloseds;

    @BindView(R.id.pull_request_progressbar)
    ProgressBar mPullRequestProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mPullRequestRecyler.setLayoutManager(mLayoutManager);
        mPullRequestRecyler.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mPullRequestRecyler.getContext(),
                mLayoutManager.getOrientation());
        mPullRequestRecyler.addItemDecoration(dividerItemDecoration);

        if (getIntent().getExtras() != null){
            Repository repository = (Repository) getIntent().getExtras().getSerializable("repository");
            setTitle(repository.getName());
            mAPI = GithubAPIFactory.getApi();
            mAPI.getPullRequests(repository.getOwner().getUsername(), repository.getName()).enqueue(pullRequestsCallback);
        }
    }


    Callback<PullRequest[]> pullRequestsCallback = new Callback<PullRequest[]>() {
        @Override
        public void onResponse(Call<PullRequest[]> call, Response<PullRequest[]> response) {
            if (response.isSuccessful()) {
                PullRequest[] pullRequests = response.body();
                List<PullRequest> pullRequestList = new ArrayList<PullRequest>(Arrays.asList(pullRequests));
                Map countByState =  PullRequestsUtils.getCountByState(pullRequestList);

                Integer count = (Integer)countByState.get("open");
                txtOpeneds.setText((count == null ? 0 : count) + " opened");
                count = (Integer)countByState.get("closed");
                txtCloseds.setText((count == null ? 0 : count) + " closed");

                mPullRequestAdapter = new PullRequestAdapter(pullRequestList);
                mPullRequestRecyler.setAdapter(mPullRequestAdapter);
                mPullRequestRecyler.setVisibility(View.VISIBLE);
                mPullRequestProgressBar.setVisibility(View.GONE);
            } else {
                Log.d("PullRequestsCallback", "Code: " + response.code() + " Message: " + response.message());
            }
        }

        @Override
        public void onFailure(Call<PullRequest[]> call, Throwable t) {
            t.printStackTrace();
        }
    };


}
