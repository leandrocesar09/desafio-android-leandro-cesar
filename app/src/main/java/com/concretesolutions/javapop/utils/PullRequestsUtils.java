package com.concretesolutions.javapop.utils;

import com.concretesolutions.javapop.models.PullRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by LeandroCesar on 26/03/2017.
 */

public class PullRequestsUtils {


    public static Map getCountByState(List<PullRequest> pullRequestList){

        Map<String, Integer> map = new HashMap<String, Integer>();
        Integer count;
        for (PullRequest temp : pullRequestList) {
            count = map.get(temp.getState());
            map.put(temp.getState(), (count == null) ? 1 : count + 1);
        }
        return map;
    }

}
