package com.concretesolutions.javapop.utils;

import java.util.List;

/**
 * Created by LeandroCesar on 25/03/2017.
 */

public class ListWrapper<T> {
    List<T> items;

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
